# Fundamentos de Ingenieria de Software

# Qué es un system on a chip

La arquitectura rápida y compleja en un sistema tradicional podemos hoy tenerlo en un disposivo pequeño gracias a un system on a chip

Un system on a chip es una CPU que también tiene una memoria RAM y un disco duro en un chip.

También incluye chips especializados que permiten realizar algunos procesos necesarios por un dispositivo móvil.

# Cómo funcionan las computadoras y los teléfonos móviles

Aprende cómo es el proceso de enviar un email, todo su recorrido desde tu computadora, servidores y notificaciones hasta que llega al destinatario.

# Qué son Bits y Bytes

Conoce la diferencia entre los Bits y Bytes, su historia, construcción y adaptación a la computación y su uso universal.

# Procesadores y arquitecturas de CPU

Vivimos en un mundo donde todo es un computador como celulares, tablets, televisores, etc.

Existen una arquitectura para computadores de escritorio y laptops, estos internamente tienen:

* **CPUs**: su velocidad de mide en GHz y tienen Cores que son el número de instrucciones en paralelo que pueden hacer.

En este momento se hacen las CPUs en silicio y estas se calientan de acuerdo a la cantidad de procesos.

* **BIOS**, es un pequeño sistema que arranca el computador y comprueba la conexión del hardware.

* **Disco duro**, es donde se almacena el sistema operativo y los archivos que guardamos.

* **Memoria RAM**, los datos del sistema operativo se envía a la memoria que sirve como intermediario con la CPU, porque es una memoria de acceso rápido, esta memoria solo funciona cuando hay electricidad.

* **GPU**, puede procesar en paralelo los pixeles de la pantalla y enviarlo en tiempo real para representarlos.

# Qué es la memoria RAM y cómo funcionan los discos duros

Sabemos que los archivos se almacenan en el disco duro y la hora de abrirlo, se cargan en memoria RAM, pero… ¿Cómo exactamente funciona eso?

Los discos duros antiguos funcionan como los vinilos antiguos: tienen un cabezal que va leyendo los datos que pasan por debajo de ella a medida que el disco gira a una velocidad constante.

Los discos duros son lentos porque deben posicionarse donde está el archivo almacenado y esto puede implicar que el brazo mecánico que mueve el cabezal se tome mucho tiempo en encontrar todos los pedazos de datos del archivo.

La memoria RAM es más rápida ya que puede acceder a los datos almacenados de manera instantánea.

Los discos duros de estado sólido no tienen el cabezal ni los discos que giran, sino que son más parecidos a las memorias RAM: funcionan electrónicamente. A pesar de eso guardan los datos en memorias flash, que son un poco más lentas que las RAM.

Los discos duros no son volátiles: guardan la información de manera persistente aunque se les quite el suministro de energía.

Además almacenan los archivos de manera secuencial: para almacenar un archivo, éste se parte en varios pedacitos y se guarda la posición de cada uno de estos pedacitos y su ubicación en el disco para poder leerlos secuencialmente.

Los sistemas de archivos son convenciones internas de los sistemas operativos para poder acceder a los archivos almacenados.

* En Linux existe ext3 o ext4
* En Windows existía FAT16 o FAT32 (File Allocation Table), que fue reemplazado por NTFS (New Technology File System)
* En Mac OSX el sistema de archivos se llamaba HFS (Hierarchical File System) pero ahora se llama AFS (Apple File System) en macOS Sierra

Cuando abrimos un archivo, el CPU se lo pide al disco duro y luego lo lleva a la memoria RAM para leerlo.

En la RAM están todos los programas y archivos que están en ejecución. Si abrimos un archivo con el Bloc de Notas, por ejemplo, ambos deben estar cargados en la RAM. Y el CPU puede acceder a la memoria RAM a través de un índice compartido, es decir, un índice que indica en qué posiciones de memoria se encuentran qué partes de un archivo o proceso.

Los datos viajan a través de un conjunto de cables paralelos llamado bus de datos, que comunica el CPU con el disco duro y la RAM para permitir la transferencia de datos.

# GPUs, tarjetas de video y sonido

Sabemos cómo los archivos se cargan en memoria pero ¿Cómo veo en pantalla que el archivo se ha abierto?

Esto se logra gracias a la Graphic Processing Unit o GPU.

La CPU puede ejecutar cualquier proceso, incluido el dibujado en pantalla de ciertos datos. Pero no es ella quien se encarga, sino la GPU: tarjetas especialmente fabricadas para realizar estas tareas.

La comunicación entre la CPU y la GPU se realiza actualmente a través de un socket llamado PCI-Express.

Estas placas de vídeo tienen sus propias unidades o núcleos de procesamiento y su propia memoria RAM.

Lo que sucede es que la GPU divide la pantalla en una matriz y cada núcleo se encarga de dibujar una parte de esa matriz, para lograr una mejor performance.

Esto es mucho más rápido de lo que podría lograr la CPU sola ya que debería dibujar pixel por pixel ella sola.

# Periféricos y sistemas de entrada de información

Los sistemas operativos normalmente tienen un núcleo llamado kernel, que es el principal elemento que los representa y es la primera parte del sistema operativo que se carga en la memoria RAM. El kernel del sistema operativo tiene acceso a todo en nuestra computadora: nuestros archivos, a nuestros periféricos, a los datos de las aplicaciones.

El kernel, inmediatamente después de ser cargado en RAM, se encarga de cargar los drivers: pequeñas piezas de software que permiten interpretar las señales eléctricas del hardware, para que el sistema operativo pueda comunicarse con ellos.

Luego tenemos otro set de drivers que pueden ser los controladores de arranque llamados drivers de aplicación. Cuanto más nos alejamos del kernel, menos privilegios tenemos. Los drivers de aplicación deben pedirle permisos a los drivers anteriores para poder acceder al hardware.

La última capa la representan las aplicaciones. Esta es la capa que menos permisos tiene, ya que las aplicaciones no deberían poder acceder al hardware directamente.

# Arquitectura de la computación

En el inicio de la computación no existía un procesador y una memoria aparte. Las computadoras estaban más cerca de ser una máquina de escribir que una de las computadoras que conocemos ahora. Eran máquinas grandes y pesadas, que requerían ser trasladadas en aviones o camiones. El código binario se escribía en tarjetas perforadas: cuyas perforaciones (o falta de ellas) representaban los 1 y 0.

Hoy en día tenemos computadoras en nuestros propios bolsillos y las cargamos a todos lados, tenemos laptops cuyos monitores se pueden desacoplar y funcionan como tablets, tenemos microchips que sirven como una computadora común y corriente.

Ese salto evolutivo en la computación ocurre gracias a la estandarización de la arquitectura de las computadoras: decidimos que un Byte son 8 bits, que la CPU es la encargada de procesar, que la GPU representa datos visualmente, que 1024 Bytes son un KiloByte, y que 1024 KB son 1 MB, que exista un puerto común como el USB que nos permite conectar otros dispositivos externos.

Estandarizamos la transferencia de datos y los protocolos de comunicación. Hay un formato definido para cada tipo de imágenes, hay una forma de escribir HTML para que el navegador lo interprete y pueda mostrarnos elementos visuales en la pantalla. Definimos una forma para comprimir un archivo.

# Introducción a las redes, protocolos e Internet

Aprende cómo tu computador se conecta a internet, qué es un switch, un router, un modem, un ISP, cómo el router sabe cuál es tu computador y de ahi, conectarse a internet.

# Puertos y protocolos de red

Como funcionan y cuales son las direcciones IP, quien las asigna a nivel publico y como los router a través de DHCP asignan las privadas.
Cuantos rangos de ip tenemos disponibles y mencionan algunos puertos y para que se usan.

# Qué es una dirección IP y el protocolo de Internet

IP es la sigla de Internet Protocol y una dirección IP es un número único con el cual una computadora o un dispositivo se identifica cuando está conectada a una red con el protocolo IP.

Cada dirección IP está compuesta por 4 números separados por puntos y son una forma de comprender números más grandes y complejos. Las direcciones IP tienen una estructura que las convierten en privadas o públicas y que además hacen parte de la máscara de red y el getaway.

Las direcciones IP permiten que cada computador o dispositivo pueda conectarse al exterior, es decir a Internet, esto a través de tecnologías como NAT o Network Address Translation.

# Cables submarinos, antenas y satelites en Internet

La mayoría de personas imaginan que el acceso a Internet consiste en conexiones satelitales, lo cual es un error, pues los satélites están destinados sólo para áreas remotas. Internet funciona a partir de cables que atraviesan diferentes lugares del mundo.

Cuando usas tu computadora o dispositivo, este se conecta a un ISP o un prestador de servicios de Internet (ese a quién le pagas tu factura). De ahí, la conexión con diferentes puntos en el mundo a través de cables submarinos, que pueden ser de fibra óptica o cobre.

Estos cables pueden comenzar en una ciudad como Nueva York y terminar en Japón y aunque no parezca, la red de Internet un poco frágil pues los cables pueden romperse por diferentes causas, como las anclas de los barcos.

# Qué es un dominio, DNS o Domain Name System

Como funciona internet, desde los nodos cables submarinos hasta la ultima milla. Como los operadores ISP filtran o restringen la velocidad de algunos contenidos.
Adicional como los sitios web usan redes CDN para distribuir contenido.

# Cómo funciona la velocidad en internet

La mayoría de los ISPs (Internet Service Providers) nos venden ancho de banda en Mb y debemos tener claro qué significa, ya que existe una importante diferencia entre Megabits y MegaBytes.

Otro aspecto importante en el funcionamiento del internet es la velocidad. A menudo confundimos la velocidad con el ancho de banda por eso debemos tener en claro que la velocidad del internet se mide obteniendo el tiempo que le toma a la información viajar a través de un punto a otro en milisegundos, a esto se le conoce como ping o latencia.

# Qué es el Modelo Cliente/Servidor

Las tecnologías utilizadas en aplicaciones web son:

Bases de datos, MySQL es una base de datos relacionales y MondoDB es una base de datos no relacional

Backend, existen muchos lenguajes que puedes usar cómo Python, Ruby, JavaScript

Servidores, existen tecnologías como NGINX, Apache, Node

Frontend, son las tecnologías que corren en el navegador, HTML, CSS y JavaScript

A un grupo de tecnologías se les conoce como Stack

Recuerda:

* Si tuvieras un código en el Frontend que se conectara a una base de datos, esta seria visible para todos.

# Cómo funciona realmente un sitio web

1. Los protocolos se encargan de manejar todas las peticiones que hacen la páginas de internet desde tu navegador hacia los servidores DNS, éstos transforman la dirección de la página web en una dirección IP y tu navegador se conecta a esa IP.

2. Una vez se tiene la dirección IP el navegador envía un HTTP request en donde envía información con las características del cliente y los requerimientos del mismo, es decir, Host requerido, página del sitio que necesita, tipo de navegador, versión del navegador, etc.

3. El servidor envía los resultados por medio del mismo protocolo HTTP en forma de un HTTP Response en donde manda todo el HTML del sitio web así como otros datos que el navegador necesita.

4. Por último se cargan los assets de nuestro sitio web y es aquí donde se descargan imágenes, sonidos, etc.

# Internet es más grande de lo que crees

TCP/IP y UDP, Tipos de Wi-fi, Firewalls, Tethering, P2P, Redes Mesh, TOR, Multi-Wan, IPs fijas vs IPs dinámicas, VPNs, TTL, Paquetes, Syncs y Ack.
Todas estas son tecnologías que se utilizan día a día que debes conocer.

# Diferencias entre Windows, Linux, Mac, iOS y Android

Windows es el sistema operativo de propósito general más usado a nivel mundial, es un sistema operativo cerrado y se encuentra en la gran mayoría de computadoras para consumidores además utiliza un núcleo propietario perteneciente a Microsoft.

Linux es el sistema operativo más utilizado en servidores, es libre y su creador Linus Torvalds aún sigue desarrollando su núcleo destacado por su alto rendimiento y alta seguridad, tienen una licencia del tipo GNU-GPL que no solo permite redistribuir si no también garantiza que las personas que redistribuyen el código deban aportar a la licencia entre otras cosas.

FreeBSD es el sistema operativo en el que está basado Mac OS y muchos sistemas embebidos así como firewalls como pfsense y más, tiene una licencia del tipo BSD…

# Permisos, niveles de procesos y privilegios de ejecución

Permisos, niveles de procesos y privilegios de ejecución
En la administración de archivos la capacidad de utilizar permisos te permite definir entre las siguientes características, los permisos existen en todos los sistemas operativos de diversas formas y se crean con las siguientes opciones:

Read ®: permisos de lectura.
Write (w): permisos de escritura.
Execute (x): permisos de ejecución.

# Fundamentos de sistemas operativos móviles

Los sistemas operativos móviles difieren a los sistemas operativos normales en seguridad, el acceso y manejo de hardware especial para telecomunicaciones, y la forma en la que se aceptan y distribuyen sus aplicaciones.

# Sistemas operativos embebidos e Internet of Things

Los sistemas embebidos son dispositivos que se encuentran en una gran variedad de lugares, estos son los sistemas de procesamiento que se utilizan en dispositivos diferentes a nuestros computadores, por ejemplo el microcontrolador que tiene programadas las secuencias de tu lavadora, el sistema embebido que tiene tu vehículo y que se encarga de coordinar tareas de seguridad básicas, entre otras cosas, el microcontrolador que tiene programadas las funciones de tu horno de microondas, el sistema de control de una estufa de inducción, la computadora embebida en un cajero automático, el sistema de navegación, estabilización y seguridad de un avión y muchos dispositivos más.

Los arduinos son una herramienta que permite crear prototipos de este tipo de sistemas, desde automatizar un horno antiguo hasta controlar una cámara por IoT, por esto son tan populares.

El Raspberry Pi es un dispositivo que contiene los mismos componentes que tiene un computador y cuesta sólo 35 USD, por esto y por sus entradas y salidas de propósito general (GPIO) es un sistema que vive y controla muchos proyectos desde sistemas retro de videojuegos hasta mini-clusters de cómputo, servidores multimedia DIY y más.

# Cómo funciona .zip: Árboles binarios

¿Cómo funciona la compresión de un archivo sin perder ningún valor de ese archivo?

Los árboles binarios nos permiten comprimir sin perder información. En este caso, vamos a comprimir “amo leer panama paper”.

1. Debemos ver cuantas veces se repite cada letra

a = 5
m = 2
r = 2
s = 1
o = 1
  = 3
p = 3
l = 1
e = e
n = 1

2. La letra con más frecuencia va a estar en el primer punto de la rama. Cuando se encuentra es 1, y cuando no se encuentra es cero

3. Con esto debemos volver a construir nuestro mensaje siguiendo eárbolol, esto quedaría

1 0001 0000001 01 00000001 001 001 000001 01 0001 1 0000000001 1 00001 1 01 0001 1 0001 001 000001 000000001

Aunque en este ejemplo no se reduce drásticamente el tamaño. Imagina párrafos más grandes u otro tipo de archivos.

# Metadatos, cabeceras y extensiones de archivos

Tu aprendiste que .jpg significa la extensión de archivos de imagen, históricamente windows tenia muy pocos bytes para la extensión, por esto la extensión normalmente era de 3 dígitos.

Existen muchas otras extensiones como .html para páginas web, .mpg4 para vídeo.

Cuando abres los archivos vas a ver su codificación binaria o dependiendo del editor puedes verlo en hexadecimal.

Un sistema operativo lee los primeros bytes del archivo para entender a que archivo corresponde, esta información se llama cabecera.

Cada sistema operativo tiene una base de datos de que programa abre que tipo de archivo.

Cuando estas transmitiendo un archivo por Internet se especifica el tipo de archivo con mime types, el cual se transmite en la cabecera de un paquete http.

# Cómo funciona el formato JPG

Asumamos que tenemos una foto de 600*800, si esto estuviera en un formato sin compresión pesaría 840KB,solo representando un color por pixel

Para tener una calidad de 32 bit la imagen debe pesar 1.9MB para esto podemos comprimir las imágenes y usar formatos como jpeg

jpg lo que hace es aproximar áreas de color, si un color esta en áreas continuas solo se declara la zona y el color de esa zona, de esta forma se pueden tener imágenes que pesan mucho menos.

# Videos, contenedores, codecs y protocolos

En un vídeo hay muchos factores para comprimir, un vídeo es si es una serie de fotos vistas muy rápido. por ejemplo si un vídeo tiene 100 frames a 24 frames/segundo y si cada frame pesara 1.9MB el vídeo pesaría más de 100MB por 4 segundos de vídeo.

Por esto en los vídeos se utilizan varias formas de compresión

**Contenedores**: es el formato es que se guarda el vídeo como .avi, .mp4, .flv, .mpg, .webm
**Codecs**: es un algoritmo que comprime un vídeo y lo descomprime como divx, h.264, ogg, vp9
**Protocolos**: es la forma de transmitir los vídeos como RTMP
**Keyframes**: cada cierta cantidad de frame existe un frame que vuelve a definir todo el área

# Qué es una red neuronal

¿Sabes qué es una red neuronal? son la base de la inteligencia artificial. Y aunque pueda parecer un concepto un poco complejo de entender, en realidad se trata de imitar por medio de software en un computador la forma en que funciona nuestro propio cerebro.

# El poder de un Raspberry Pi

El Raspberry Pi, la revolucionaria computadora de 35 dólares que se lanzó hace 5 años, ha roto las barreras que le impedían a muchos desarrolladores, hobbistas y estudiantes realizar proyectos que involucran el uso de hardware. Pero ¿sabes qué es? ¿Por qué se volvió tan popular?

En este Platzi Live Ricardo Celis Santiago (@celismx), miembro del team Platzi, te explica para qué nace, cómo está hecho y además te muestra un par de proyectos increíbles hechos por usuarios de Raspberry Pi y que tú mismo podrías hacer. Cuéntanos en los comentarios si te interesa hacer algún proyecto y, de ser así, ¿cuál sería?
